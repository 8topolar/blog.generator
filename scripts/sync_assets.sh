#!/usr/bin/env bash
[[ -d assets ]] && rm -rf assets

mkdir assets

while read fullpath; do

  while read cid; do
    (cd assets; ipfs get $cid)
  done < <(grep -Eo "\/assets\/[^\"]+" "$fullpath" | sed 's/\/assets\///')

done < <(grep -rl "/assets/" src/posts)
