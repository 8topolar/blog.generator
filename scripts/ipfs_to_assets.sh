#!/usr/bin/env bash

file_list=`grep -rl "<%GATEWAY_PREFIX%>" src/posts`

for fullpath in $file_list; do
  sed -i 's/<%GATEWAY_PREFIX%>\/ipfs/\/assets/g' "$fullpath"
done
